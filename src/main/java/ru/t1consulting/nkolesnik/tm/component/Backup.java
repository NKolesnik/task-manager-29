package ru.t1consulting.nkolesnik.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.command.data.DataBackupSaveCommand;

public final class Backup extends Thread{

    @NotNull final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap){
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true){
            Thread.sleep(3000);
            bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
        }
    }

}
