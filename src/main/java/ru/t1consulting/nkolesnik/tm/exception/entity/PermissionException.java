package ru.t1consulting.nkolesnik.tm.exception.entity;

public final class PermissionException extends AbstractEntityNotFoundException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}
