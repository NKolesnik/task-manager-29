package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @NotNull
    Boolean isAuth();

    @NotNull
    String getUserId();

    @Nullable
    User getUser();

    void checkRoles(@Nullable Role[] roles);

}
